from setuptools import setup


setup(
    name="muhammad fahmy",
    packages=["hello_world"],
    description="Testing First Module!",
    version="0.1",
    url="http://github.com/testdrivenio/private-pypi/sample-package",
    author="Muhammad Fahmy",
    author_email="muhammadfahmy123@gmail.com",
    keywords=["pip", "pypi"],
)
